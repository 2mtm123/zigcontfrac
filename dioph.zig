const std = @import("std");
const builtin = @import("builtin");
const ArrayList = std.ArrayList;
const process = std.process;
const libcontfrac = @import("libcontfrac/src/main.zig");
const ContFracIter = libcontfrac.ContFracIter;


pub fn main() !void {
    const writer = std.io.getStdOut().writer();

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var allocator = arena.allocator();

    var args = try parseArgs(allocator);
    var list = RowList.init(allocator);
    defer list.deinit();

    var cont = ContFracIter(i64){ .numerator = args.a, .denumerator = args.b };
    var P: i64 = cont.next() orelse return err_zero();
    var Q: i64 = 1;
    var Pp: @TypeOf(P) = 1;
    var Qp: @TypeOf(Q) = 0;

    var n: i64 = 0;
    while (cont.next()) |q| : (n += 1) {
        inIterFunc(&P, &Pp, q); 
        inIterFunc(&Q, &Qp, q);
        switch (args.printOption) {
            PrintOption.table => try list.append(.{ .P = P, .Q = Q }),
            PrintOption.list => try writer.print("n: {}\nP: {}\nQ: {}\n", .{n, P, Q}),
            else => {},
        }
    }

    if (args.printOption == PrintOption.table) { 
        var P_width: usize = countDigits(P) + 1;
        var Q_width: usize = countDigits(Q) + 1;
        var n_width: usize = countDigits(n) + 1;
        try printTable(n_width, P_width, Q_width, list);
    }

    // TODO: make more readable
    // TODO: make neparate inline func for if statement
    var sign = if (@mod(n - 1, 2) == 1) "-" else "+";
    var second_sign = if (libcontfrac.sign(args.b) < 0) "-" else "+";
    args.b = try std.math.absInt(args.b);

    try writer.print(
        "x = {s}{} {s} {}t\n",
        .{ sign, args.c * Qp, second_sign, args.b }
    );
    
    sign = if (@mod(n, 2) == 1) "-" else "+";
    second_sign = if (libcontfrac.sign(-args.a) < 0) "-" else "+";
    args.a = try std.math.absInt(args.a);
    
    try writer.print(
        "y = {s}{} {s} {}t\n",
        .{ sign, args.c * Pp, second_sign, args.a }
    );

    if (args.shouldWait) {
        const reader = std.io.getStdIn().reader();
        try writer.print("Натисність будь яку клавішу\n", .{});
        _ = try reader.readByte();
    }
}


fn horizontalLine(width: usize) !void {
    const writer = std.io.getStdOut().writer();
    var n: usize = 0;
    return while (n < width) : (n += 1) {
        try writer.print("─", .{});
    };
}


fn printHead(
    n_width: usize,
    P_width: usize,
    Q_width: usize,
) !void {
    const writer = std.io.getStdOut().writer();
    
    try writer.print("┌", .{});
    try horizontalLine(n_width);
    try writer.print("┬", .{});
    try horizontalLine(P_width);
    try writer.print("┬", .{});
    try horizontalLine(Q_width);
    try writer.print("┐\n", .{});
    
    try writer.print("│", .{});
    try std.fmt.formatBuf("N", .{ .width = n_width }, writer);
    try writer.print("│", .{});
    try std.fmt.formatBuf("P", .{ .width = P_width }, writer);
    try writer.print("│", .{});
    try std.fmt.formatBuf("Q", .{ .width = Q_width }, writer);
    try writer.print("│\n", .{});   
}


fn printRows(
    n_width: usize,
    P_width: usize,
    Q_width: usize,
    list: RowList
) !void {
    const writer = std.io.getStdOut().writer();

    var n: usize = 0;
    return while (n < list.items.len) : (n += 1) {
        try writer.print("├", .{});
        try horizontalLine(n_width);
        try writer.print("┼", .{});
        try horizontalLine(P_width);
        try writer.print("┼", .{});
        try horizontalLine(Q_width);
        try writer.print("┤\n", .{});

        try writer.print("│", .{});
        try std.fmt.formatIntValue(n + 1, "d", .{ .width = n_width }, writer);
        try writer.print("│", .{});
        try std.fmt.formatIntValue(list.items[n].P, "d", .{ .width = P_width }, writer);
        try writer.print("│", .{});
        try std.fmt.formatIntValue(list.items[n].Q, "d", .{ .width = Q_width }, writer);
        try writer.print("│\n", .{});
    };
}

fn printBottom(
    n_width: usize,
    P_width: usize,
    Q_width: usize,
) !void {
    const writer = std.io.getStdOut().writer();

    try writer.print("└", .{});
    try horizontalLine(n_width);
    try writer.print("┴", .{});
    try horizontalLine(P_width);
    try writer.print("┴", .{});
    try horizontalLine(Q_width);
    try writer.print("┘\n\n", .{});
}


inline fn printTable(
    n_width: usize,
    P_width: usize,
    Q_width: usize,
    list: RowList
) !void { 
    try printHead(n_width, P_width, Q_width);
    try printRows(n_width, P_width, Q_width, list);
    try printBottom(n_width, P_width, Q_width);
}

// TODO: change name
fn inIterFunc(n: *i64, np: *i64, q: i64) void {
    // n, np = (n*q + np), n
    const temp: i64 = n.*;
    n.* *= q;
    n.* += np.*;
    np.* = temp;
} 


const ParsedArgs = struct {
    a: i64 = 0,
    b: i64 = 0,
    c: i64 = 0,
    printOption: PrintOption = PrintOption.table,
    shouldWait: bool = false,
};


const PrintOption = enum {
    none,
    table,
    list,
};


fn getInt(comptime str: []const u8) !i64 {
    const reader = std.io.getStdIn().reader();
    const writer = std.io.getStdOut().writer();
    // trim annoying windows-only carriage return character
    while (true) {
        try writer.print(str, .{});
        var buf: [20]u8 = undefined;
        const len = try reader.read(&buf);


        var line = std.mem.trimRight(u8, buf[0..len], "\n");

        if (builtin.os.tag == .windows) {
            line = std.mem.trimRight(u8, line, "\r");
        }

        if (std.fmt.parseInt(i64, line, 10)) |res| {
            return res;
        } else |_| {
            try writer.print("Неправильний формат, спробуйте ще раз\n", .{});
        }
    }
}


fn parseArgs(allocator: std.mem.Allocator) !ParsedArgs {
    var res = ParsedArgs{};

    var args = try process.argsAlloc(allocator);
    defer process.argsFree(allocator, args);

    if (args.len == 1) {
        res.a = try getInt("Введіть параметр a: ");
        res.b = try getInt("Введіть параметр b: ");
        res.c = try getInt("Введіть параметр c: ");
        res.shouldWait = true;
        return res;
    }

    const arr = [_]*i64{ &res.a, &res.b, &res.c };
    var p: usize = 0;
    for (args[1..args.len]) |arg| {
        if (arg[0] == '-') {
            for (arg[1..arg.len]) |l| {
                switch (l) {
                    'l' => res.printOption = PrintOption.list,
                    't' => res.printOption = PrintOption.table,
                    'w' => res.shouldWait = true,
                    else => res.printOption = PrintOption.none,
                }
            }
        } else {
            if (p == arr.len) {
                try help(args[0]);
                process.exit(1);
            }

            arr[p].* = try std.fmt.parseInt(i64, arg, 10);
            p += 1;
        }
    }

    if (p != arr.len) {
        process.exit(1);
    }
    
    return res;
}


const RowElements = struct {
    P: i64,
    Q: i64,
};
const RowList = ArrayList(RowElements);


fn countDigits(num: anytype) usize {
    var n: i64 = std.math.absInt(num) catch 0;
    var count: usize = 0;
    while (true) {
        n = @divFloor(n, 10);
        count += 1;
        if (n == 0)
            return count;
    }
}


fn err_zero() !void {
    const writer = std.io.getStdOut().writer();
    return writer.print("b не має дорівнювати нулю\n", .{});
}

// TODO: change printed text
fn help(name: []const u8) !void {
    const writer = std.io.getStdOut().writer();
    return writer.print("Використання: {s} a b c\n", .{name});
}
