const std = @import("std");
const math = std.math;
const testing = std.testing;
const expect = testing.expect;

const ContFracError = error {
    ZeroDenumerator,
};


pub inline fn sign(i: anytype) @TypeOf(i) {
    const T = @TypeOf(i);
    return switch (@typeInfo(T)) {
        .Int, .ComptimeInt => @as(T, @boolToInt(i > 0)) - @boolToInt(i < 0),
        .Float, .ComptimeFloat => @intToFloat(T, @boolToInt(i > 0)) - @intToFloat(T, @boolToInt(i < 0)),
        .Vector => |vinfo| blk: {
            switch (@typeInfo(vinfo.child)) {
                .Int, .Float => {
                    const zero = @splat(vinfo.len, @as(vinfo.child, 0));
                    const one = @splat(vinfo.len, @as(vinfo.child, 1));
                    break :blk @select(vinfo.child, i > zero, one, zero) - @select(vinfo.child, i < zero, one, zero);
                },
                else => @compileError("Expected vector of ints or floats, found " ++ @typeName(T)),
            }
        },
        else => @compileError("Expected an int, float or vector of one, found " ++ @typeName(T)),
    };
}


pub fn ContFracIter(comptime T: type) type {
    return struct {
        const Self = @This();
        numerator: T,
        denumerator: T,
        
        inline fn areSignEqual(a: T, b: T) bool {
            return sign(a) == sign(b);
        }

        pub fn next(self: *Self) ?T {
            if (self.denumerator == 0) {
                return null;
            }
            
            defer {
                const temp: T = self.denumerator;
                self.denumerator = @mod(self.numerator, self.denumerator);
                self.numerator = temp;
            }

            if (!areSignEqual(self.numerator, self.denumerator)) {
                self.numerator = math.absInt(self.numerator) catch 0;
                defer self.numerator = self.denumerator - self.numerator;
                self.denumerator = math.absInt(self.denumerator) catch 0;
                                
                return -(@divFloor(self.numerator, self.denumerator) + 1);
            }
            
            return @divFloor(self.numerator, self.denumerator);
        }
    };
}


test "ContFracIter" {
    var c = ContFracIter(i32){.numerator=975, .denumerator=839};

    try expect(c.next().? == 1);
    try expect(c.next().? == 6);
    try expect(c.next().? == 5);
    try expect(c.next().? == 1);
    try expect(c.next().? == 10);
    try expect(c.next().? == 2);
    try expect(c.next() == null);
}

test "ContFracIter negative" {
    {
        var c = ContFracIter(i32){.numerator=-602, .denumerator=367};
        try expect(c.next().? == -2);
        try expect(c.next().? == 2);
        try expect(c.next().? == 1);
        try expect(c.next().? == 3);
        try expect(c.next().? == 1);
        try expect(c.next().? == 1);
        try expect(c.next().? == 4);
        try expect(c.next().? == 3);
        try expect(c.next() == null);
    }
    {
        var c = ContFracIter(i32){.numerator=-102, .denumerator=203};
        try expect(c.next().? == -1);
        try expect(c.next().? == 2);
        try expect(c.next().? == 101);
        try expect(c.next() == null);
    }
}


pub fn ContFrac(comptime T: type) type {
    return struct {
        const Self = @This();
        elements: ?[]T = null,

        pub fn from_integer_ratio(
            self: *Self,
            numerator: anytype,
            denumerator: anytype,
            allocator: std.mem.Allocator
        ) !?[]T {
            var iter = ContFracIter(T){
                .numerator=numerator,
                .denumerator=denumerator
            };
            var array = std.ArrayList(T).init(allocator);
            defer array.deinit();

            if (denumerator == 0) {
                return ContFracError.ZeroDenumerator;
            }

            while (iter.next()) |res| {
                try (array.append(res));
            }

            self.elements = array.toOwnedSlice();
            return self.elements;
        }
        // TODO: init from string & float
    };
}

test "ContFrac.from_integer_ratio" {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var a = arena.allocator();

    var contfrac = ContFrac(i32){};

    {
        var items = (try contfrac.from_integer_ratio(975, 839, a)) orelse &[_]i32{};

        try testing.expectEqualSlices(i32, &[_]i32{1, 6, 5, 1, 10, 2}, items);
    }

    {
        var items = (try contfrac.from_integer_ratio(1, 5, a)) orelse &[_]i32{};

        try testing.expectEqualSlices(i32, &[_]i32{0, 5}, items);
    }

    {
        var items = (try contfrac.from_integer_ratio(2, 5, a)) orelse &[_]i32{};

        try testing.expectEqualSlices(i32, &[_]i32{0, 2, 2}, items);
    }

    {
        try testing.expectError(ContFracError.ZeroDenumerator, contfrac.from_integer_ratio(100, 0, a));
    }
}

/// Pi
pub fn GenContFracIterPi(comptime T: type) type {
    return struct {
        const Self = @This();
        count: i32 = 0,
        current: i32 = 0,

        pub fn next(self: *Self) ?[2]T {
            if (self.current == self.count) {
                return null;
            }

            if (self.current == 0) {
                self.current += 1;
                return [2]T{ 0, 4 };
            }

            defer self.current += 1;
            return [2]T{ self.current * 2 - 1, self.current * self.current };
        }
    };
}

test "GenContFracIterPi" {
    var iter_pi = GenContFracIterPi(i32){ .count = 5 };

    try expect(std.mem.eql(i32, &[_]i32{0, 4}, &(iter_pi.next().?)));
    try expect(std.mem.eql(i32, &[_]i32{1, 1}, &(iter_pi.next().?)));
    try expect(std.mem.eql(i32, &[_]i32{3, 4}, &(iter_pi.next().?)));
    try expect(std.mem.eql(i32, &[_]i32{5, 9}, &(iter_pi.next().?)));
    try expect(std.mem.eql(i32, &[_]i32{7, 16}, &(iter_pi.next().?)));
    try expect(iter_pi.next() == null);
}

/// Phi, golden ratio or (1 + sqrt(5)) / 2
pub fn ContFracIterPhi(comptime T: type) type {
    return struct {
        const Self = @This();
        count: i32 = 0,
        current: i32 = 0,

        pub fn next(self: *Self) ?T {
            if (self.current == self.count) {
                return null;
            }

            self.current += 1;
            return 1;
        }
    };
}

test "ContFracIterPhi" {
    var iter_phi = ContFracIterPhi(i32){ .count = 10 };

    var i: usize = 0;
    while (i < 10) : (i += 1) {
        try expect(iter_phi.next().? == 1);
    }

    try expect(iter_phi.next() == null);
}

/// Euler constant
pub fn ContFracIterE(comptime T: type) type {
    return struct {
        const Self = @This();
        count: i32 = 0,
        current: i32 = 0,
        stage: T = 0,

        pub fn next(self: *Self) ?T {
            if (self.current == self.count) {
                return null;
            }
            defer self.current += 1;

            if (self.current == 0) {
                return 2;
            }
                
            if (@mod(self.current, 3) != 2) {
                return 1;
            } else {
                self.stage += 2;
                return self.stage;
            }
        }
    };
}


test "ContFracIterE" {
    var iter_e = ContFracIterE(i32){ .count = 10 };

    var i: usize = 0;
    var a: [10]i32 = undefined;

    while (i < 10) : (i += 1) {
        a[i] = iter_e.next().?;
    }

    try expect(std.mem.eql(i32, &[_]i32{2, 1, 2, 1, 1, 4, 1, 1, 6, 1}, &a));
    try expect(iter_e.next() == null);
}
